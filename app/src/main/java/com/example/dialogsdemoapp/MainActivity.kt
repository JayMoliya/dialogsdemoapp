package com.example.dialogsdemoapp

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showAlertDialog()

        showDatePicker()

        showTimePicker()

        btnCustomDialog.setOnClickListener {
            val dialog = CustomDialogFragment()

            dialog.show(supportFragmentManager,"dialog")
        }

    }

    private fun showTimePicker() {

        btnTimePicker.setOnClickListener {

            val cal = Calendar.getInstance()
            val hour = cal.get(Calendar.HOUR)
            val minute = cal.get(Calendar.MINUTE)

            val timePicker = TimePickerDialog(this, { _, h, m ->
                textView.text = "Time :- ${h.toString()} : $m"

            }, hour, minute, false)

            timePicker.show()

        }

    }

    private fun showDatePicker() {

        btnDatePicker.setOnClickListener {

            val cal = Calendar.getInstance()
            val year = cal.get(Calendar.YEAR)
            val month = cal.get(Calendar.MONTH)
            val day = cal.get(Calendar.DAY_OF_MONTH)

            val datePicker = DatePickerDialog(this, { _, year, monthOfYear, dayOfMonth ->

                textView.text = "Date : $dayOfMonth/${monthOfYear + 1}/$year"

            }, year, month, day)

            datePicker.show()

        }
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this)
            .setTitle("AlertDialogExample")
            .setMessage("Do you want to close this Application?")
            .setPositiveButton("Yes") { _, _ ->
                finish()
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }.create()

        btnAlertDialog.setOnClickListener {
            alertDialog.show()
        }
    }
}