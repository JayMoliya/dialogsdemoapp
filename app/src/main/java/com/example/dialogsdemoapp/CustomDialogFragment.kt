package com.example.dialogsdemoapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment


class CustomDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val  view =  inflater.inflate(R.layout.fragment_custom_dialog, container, false)

        val cancelButton = view.findViewById<Button>(R.id.btnCancel)
        val submitButton = view.findViewById<Button>(R.id.btnOk)
        val editText = view.findViewById<EditText>(R.id.editTextInput)

        cancelButton.setOnClickListener {
            dismiss()
        }

        submitButton.setOnClickListener {
            val inputValue = editText.text.toString()
            if(inputValue.isNotEmpty()){
                Toast.makeText(context, inputValue, Toast.LENGTH_SHORT).show()
                dismiss()
            }
            else{
                Toast.makeText(context, "Please Enter any Value", Toast.LENGTH_SHORT).show()
                dismiss()

            }
        }

       return view

    }
}